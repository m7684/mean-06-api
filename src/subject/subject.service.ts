import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Subject } from 'src/entities/subject.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SubjectService {
  constructor(
    @InjectRepository(Subject) private subjectRepo: Repository<Subject>,
  ) {}

  all(): Promise<Subject[]> {
    return this.subjectRepo.find();
  }

  byId(id: number): Promise<Subject> {
    return this.subjectRepo.findOne(id);
  }
}
