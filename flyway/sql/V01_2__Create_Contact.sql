CREATE TABLE tb_contact (
    id int not null auto_increment,
    con_subject int,
    con_message varchar(300),
    con_status varchar(1),
    con_comment varchar(300),
    create_date date,
    create_by_ip varchar(150),
    update_date date,
    update_by int,
    PRIMARY KEY (id)
);
